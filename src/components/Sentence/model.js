const { Schema } = require('mongoose');
const connections = require('../../config/connection');

const SentenceSchema = new Schema(
    {
        sentence: {
            type: String,
            trim: true,
        }
    },
    {
        collection: 'sentencemodel',
        versionKey: false,
    },
);

module.exports = connections.model('SentenceModel', SentenceSchema);
