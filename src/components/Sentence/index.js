const SentenceService = require('./service');

async function findAll(req, res, next) {
    try {
        const sentences = await SentenceService.findAll();

        res.status(200).json({
            data: sentences,
        });
    } catch (error) {
        res.status(500).json({
            error: error.message,
            details: null,
        });

        next(error);
    }
}

async function findById(req, res, next) {
    try {
        const sentence = await SentenceService.findById(req.params.id);

        return res.status(200).json({
            data: sentence,
        });
    }
    catch (error) {
        res.status(500).json({
            error: error.message,
            details: null,
        });

        next(error);
    }
}

async function create(req, res, next) {
    try {
        const sentence = await SentenceService.create(req.body);

        return res.status(200).json({
            data: sentence,
        });
    } catch (error) {
        res.status(500).json({
            message: error.name,
            details: error.message,
        });

        return next(error);
    }
}

async function findLongestWordById(req, res, next) {
    try {
        const sentenceObject = await SentenceService.findById(req.params.id);
        const sentence = sentenceObject.sentence;
        let longestWord = '';
        let longestWordLength = 0;
        const sentencesArray = sentence.split(' ');
        sentencesArray.forEach(function(word) {
            if (word.length >= longestWordLength) {
                longestWord = word;
                longestWordLength = word.length;
            }
        });

        return res.status(200).json({
            data: longestWord,
        });
    } catch (error) {
        res.status(500).json({
            message: error.name,
            details: error.message,
        });

        return next(error);
    }
}

module.exports = {
    findAll,
    findById,
    create,
    findLongestWordById,
};
