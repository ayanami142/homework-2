const SentenceModel = require('./model');

function findAll() {
    return SentenceModel.find({}).exec();
}

function findById(id) {
    return SentenceModel.findById(id).exec();
}

function create(profile) {
    return SentenceModel.create(profile);
}

function findLongestWorldById(id) {
    return id;
}

module.exports = {
    findAll,
    findById,
    create,
    findLongestWorldById,
};
