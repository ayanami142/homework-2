const { Router } = require('express');
const SentenceComponent = require('../Sentence');

const router = Router();

router.get('/', SentenceComponent.findAll);
router.get('/:id', SentenceComponent.findById);
router.post('/', SentenceComponent.create);
router.get('/:id/longestWorld', SentenceComponent.findLongestWordById);

module.exports = router;
